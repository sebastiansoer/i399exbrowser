(function () {
    'use strict';

    var ul = document.getElementById('list');
    var button = document.getElementById('button1');
    var input = document.getElementById('input1');

    var counter = 1;
    var items = [new Item(counter++, 'item 1'), new Item(counter++, 'item 2')];

    render();

    function render() {

    }

    function Item(id, text) {
        this.id = id;
        this.text = text;
    }

    function addLi(item) {
        var li = document.createElement('li');
        li.innerText = item.text;
        ul.appendChild(li);
    }

})();
